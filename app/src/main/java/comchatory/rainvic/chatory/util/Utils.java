package comchatory.rainvic.chatory.util;

import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by rainvic on 7/8/17.
 */

public class Utils {

    public static long getCurrentUTCTimeStamp(){

        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        Calendar calendar = Calendar.getInstance(timeZone);

        return calendar.getTimeInMillis();
    }

}
