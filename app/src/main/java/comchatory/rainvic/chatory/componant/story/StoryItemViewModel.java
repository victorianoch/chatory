package comchatory.rainvic.chatory.componant.story;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;

import java.util.Calendar;

import comchatory.rainvic.chatory.data.model.StoryLine;
import comchatory.rainvic.chatory.data.source.FirebaseStrg;

/**
 * Created by rainvic on 6/28/17.
 */

public class StoryItemViewModel extends BaseObservable {
    private Context context;
    private StoryLine storyLine;
    private String profileImg;

    public StoryItemViewModel(Context context, StoryLine storyLine, String profileImg) {
        this.context = context;
        this.storyLine = storyLine;
        this.profileImg = profileImg;
    }

    @Bindable
    public String getWho(){
        if(storyLine == null)
            return null;

        return storyLine.getWho();
    }

    @Bindable
    public String getContent(){
        if(storyLine == null)
            return null;

        return storyLine.getContent();
    }

    @Bindable
    public String getContentImg(){
        if(storyLine == null)
            return null;

        return storyLine.getContentImg();
    }

    @Bindable
    public int getContentImgVisibility(){
//        if(storyLine == null || storyLine.getContentImg() == null || storyLine.getContentImg().isEmpty())
//            return View.GONE;
//
//        return View.VISIBLE;
        return View.GONE;
    }

    @Bindable
    public String getProfileImg(){
        return profileImg;
    }

    @Bindable
    public String getModifiedAt(){
        if(storyLine == null)
            return null;
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(storyLine.getModifiedAt());

        // TODO: 6/28/17
        return "06:23";
    }

    @BindingAdapter("bind:setProfileImg")
    public static void setProfileImg(ImageView imageView, String v) {

//        Glide.with(imageView.getContext())
//                .load(v)
//                .placeholder(R.drawable.default_avatar)
//                .crossFade()
//                .centerCrop()
//                .into(imageView);

        FirebaseStrg firebaseStrg = FirebaseStrg.getINSTANT(imageView.getContext());



        Glide.with(imageView.getContext())
                .using(new FirebaseImageLoader())
                .load(firebaseStrg.getAvatarImgRef(v))
                .crossFade()
                .centerCrop()
                .into(imageView);

    }

    @BindingAdapter("bind:setContentImg")
    public static void setContentImg(ImageView imageView, String v) {

        Glide.with(imageView.getContext())
                .load(v)
                .crossFade()
                .centerCrop()
                .into(imageView);

    }
}
