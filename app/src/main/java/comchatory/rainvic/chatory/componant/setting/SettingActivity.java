package comchatory.rainvic.chatory.componant.setting;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import comchatory.rainvic.chatory.BaseActivity;
import comchatory.rainvic.chatory.R;
import comchatory.rainvic.chatory.databinding.ActivitySettingBinding;

public class SettingActivity extends BaseActivity {

    private ActivitySettingBinding binding;
    private SettingViewModel settingViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_setting);

        settingViewModel = new SettingViewModel();
        binding.setViewModel(settingViewModel);

//        setContentView(R.layout.activity_setting);
    }
}
