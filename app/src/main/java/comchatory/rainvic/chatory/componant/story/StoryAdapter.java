package comchatory.rainvic.chatory.componant.story;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import comchatory.rainvic.chatory.R;
import comchatory.rainvic.chatory.data.model.Character;
import comchatory.rainvic.chatory.data.model.StoryLine;
import comchatory.rainvic.chatory.databinding.ItemStoryLineLeftBinding;
import comchatory.rainvic.chatory.databinding.ItemStoryLineRightBinding;
import comchatory.rainvic.chatory.viewUtil.HeaderRecyclerViewAdapterV2;

/**
 * Created by rainvic on 6/28/17.
 */

public class StoryAdapter extends HeaderRecyclerViewAdapterV2<StoryAdapter.BindingHolder> {

    private Context context;
    private List<StoryLine> storyLineList;

    private Character lead;
    private Map<String, Character> charactersMap = new HashMap<>();

    private static final int VIEWTYPE_LEAD = Integer.MIN_VALUE + 10;
    private static final int VIEWTYPE_OTHERS = Integer.MIN_VALUE + 11;

    public StoryAdapter(Context context, Character lead, List<Character> characters, List<StoryLine> storyLineList) {
        this.context = context;
        this.storyLineList = storyLineList;

        this.lead = lead;

        for (Character chr :
                characters) {
            charactersMap.put(chr.getCharacterName(), chr);
        }
    }

    /********************************************************
    Header
    *********************************************************/
    @Override
    public boolean useHeader() {
        return false;
    }

    @Override
    public BindingHolder onCreateHeaderViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindHeaderView(BindingHolder holder, int position) {

    }

    /********************************************************
    Footer
    *********************************************************/
    @Override
    public boolean useFooter() {
        return false;
    }

    @Override
    public BindingHolder onCreateFooterViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindFooterView(BindingHolder holder, int position) {

    }

    /********************************************************
    Basic
    *********************************************************/
    @Override
    public BindingHolder onCreateBasicItemViewHolder(ViewGroup parent, int viewType) {

        if(viewType == VIEWTYPE_LEAD) {
            ItemStoryLineRightBinding itemStoryLineRightBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.getContext()),
                    R.layout.item_story_line_right,
                    parent,
                    false
            );

            return new BindingHolder(itemStoryLineRightBinding);
        }else {

            ItemStoryLineLeftBinding itemStoryLineLeftBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.getContext()),
                    R.layout.item_story_line_left,
                    parent,
                    false
            );

            return new BindingHolder(itemStoryLineLeftBinding);
        }
    }

    @Override
    public void onBindBasicItemView(BindingHolder holder, int position) {

        final StoryLine storyLine = (StoryLine) getBasicItem(position);
        if(storyLine != null){
            if(getBasicItemType(position) == VIEWTYPE_LEAD) {
                ItemStoryLineRightBinding binding = holder.itemStoryLineRightBinding;
                binding.setViewModel(new StoryItemViewModel(context, storyLine, lead.getAvatar()));
            }else{
                ItemStoryLineLeftBinding binding = holder.itemStoryLineLeftBinding;
                Character character = charactersMap.get(storyLine.getWho());
                binding.setViewModel(new StoryItemViewModel(context, storyLine, character==null?"":character.getAvatar()));
            }
        }
    }

    @Override
    public int getBasicItemCount() {
        if(storyLineList == null)
            return 0;

        return storyLineList.size();
    }

    @Override
    public int getBasicItemType(int position) {
        if(getBasicItem(position) != null && ((StoryLine)getBasicItem(position)).getWho().equals(lead.getCharacterName()))
            return VIEWTYPE_LEAD;
        else
            return VIEWTYPE_OTHERS;
    }

    @Override
    public Object getBasicItem(int position) {
        if(storyLineList == null || storyLineList.isEmpty())
            return null;

        return storyLineList.get(position);
    }

    public void addNewStoryLines(List<StoryLine> newlyAdded){
        if(storyLineList == null)
            storyLineList =  new ArrayList<>();

//        storyLineList.addAll(newlyAdded);
//        notifyDataSetChanged();
        for (StoryLine sl :
                newlyAdded) {
            storyLineList.add(sl);
            notifyItemInserted(storyLineList.size()-1);
        }
    }

    static class BindingHolder extends RecyclerView.ViewHolder {
        private ItemStoryLineLeftBinding itemStoryLineLeftBinding;
        private ItemStoryLineRightBinding itemStoryLineRightBinding;

        public BindingHolder(ItemStoryLineLeftBinding binding) {
            super(binding.getRoot());
            this.itemStoryLineLeftBinding = binding;

        }

        public BindingHolder(ItemStoryLineRightBinding binding) {
            super(binding.getRoot());
            this.itemStoryLineRightBinding = binding;

        }
    }

}
