package comchatory.rainvic.chatory.componant.story;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewTreeObserver;

import java.util.ArrayList;
import java.util.List;

import comchatory.rainvic.chatory.BaseActivity;
import comchatory.rainvic.chatory.R;
import comchatory.rainvic.chatory.componant.setting.SettingActivity;
import comchatory.rainvic.chatory.data.model.Character;
import comchatory.rainvic.chatory.data.model.Story;
import comchatory.rainvic.chatory.data.model.StoryLine;
import comchatory.rainvic.chatory.databinding.ActivityStoryBinding;
import comchatory.rainvic.chatory.util.Utils;
import comchatory.rainvic.chatory.viewUtil.MyLinearLayoutManager;

import static comchatory.rainvic.chatory.data.model.StoryLine.CONTENT_TYPE_TEXT;

public class StoryActivity extends BaseActivity
//        implements View.OnFocusChangeListener
{

    private ActivityStoryBinding binding;

    private StoryAdapter storyAdapter;

    private StoryViewModel viewModel;

    private List<StoryLine> storyLines;

    private boolean isSoftKeyBoardUp = false;

    private List<Character> characterList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_story);

        setSupportActionBar(binding.tbStory);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        setTitle("희재");

        /********************************************************
        TESTING DATA
        *********************************************************/
        Story story = new Story("Story one", 1000l, Utils.getCurrentUTCTimeStamp(), Utils.getCurrentUTCTimeStamp(), 999l, 555l, 354l);

        for (int i=0; i<5; i++){
            Character crt = new Character("Chrt"+i, Utils.getCurrentUTCTimeStamp(), "char_"+i+".jpg");
            characterList.add(crt);
        }

        List<Character> nonLead = new ArrayList<>();
        nonLead.addAll(characterList);
        nonLead.remove(0);


        initStory();

        viewModel = new StoryViewModel(this, story, characterList.get(0), nonLead, new StoryViewModel.StoryLineUpdateCallBack() {
            @Override
            public void onStoryLineUpdated(List<StoryLine> newStoryLines) {
                if(storyAdapter != null){
                    storyAdapter.addNewStoryLines(newStoryLines);
                    binding.rvChatory.smoothScrollToPosition(storyAdapter.getBasicItemCount()-1);
                }
            }

            @Override
            public void onStoryLineUpdatedForNewStoryLines(List<StoryLine> storyLines) {

            }
        });

        binding.setViewModel(viewModel);
//        binding.etTyping.setOnFocusChangeListener(this);


        binding.getRoot().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect measureRect = new Rect(); //you should cache this, onGlobalLayout can get called often
                binding.getRoot().getWindowVisibleDisplayFrame(measureRect);
                // measureRect.bottom is the position above soft keypad
                int keypadHeight = binding.getRoot().getRootView().getHeight() - measureRect.bottom;

                if (keypadHeight > 0) {
                    // keyboard is opened
                    if(storyAdapter != null && !isSoftKeyBoardUp){
                                binding.rvChatory.scrollToPosition(storyAdapter.getBasicItemCount()-1);
                                isSoftKeyBoardUp = true;
                    }
                } else {
                    //store keyboard state to use in onBackPress if you need to
//                    mMyDependentView.setVisibility(View.VISIBLE);
                    isSoftKeyBoardUp = false;
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(storyAdapter != null){
            binding.rvChatory.scrollToPosition(storyAdapter.getBasicItemCount()-1);
        }
    }

    private void initStory(){

        storyLines = new ArrayList<>();
//        for (int i=0; i<15; i++){
//            StoryLine storyLine = new StoryLine("원준" + (i%2 == 1?"보기":""), CONTENT_TYPE_TEXT, "채팅장의 글씨로 검색하시겠습니까", "test", 1498746913780l, 1498746921780l);
//            storyLines.add(storyLine);
//        }


        List<Character> nonLead = new ArrayList<>();
        nonLead.addAll(characterList);
        nonLead.remove(0);

        storyAdapter = new StoryAdapter(this, characterList.get(0), nonLead, storyLines);

        binding.rvChatory.setHasFixedSize(false);
        binding.rvChatory.setAdapter(storyAdapter);
        binding.rvChatory.setLayoutManager(new MyLinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }

//    @Override
//    public void onFocusChange(View view, boolean b) {
//        if(view != null && b && view instanceof EditText){
//            if(storyAdapter != null){
//                final Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        binding.rvChatory.smoothScrollToPosition(storyAdapter.getBasicItemCount()-1);
//                    }
//                }, 300);
//            }
//        }
//    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_story, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_setting:
                Intent intent = new Intent(StoryActivity.this, SettingActivity.class);
                startActivity(intent);
                return true;

//            case R.id.action_favorite:
                // User chose the "Favorite" action, mark the current item
                // as a favorite...
//                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
}
