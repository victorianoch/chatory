package comchatory.rainvic.chatory.componant.story;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.android.databinding.library.baseAdapters.BR;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import comchatory.rainvic.chatory.data.model.Character;
import comchatory.rainvic.chatory.data.model.StoryLine;
import comchatory.rainvic.chatory.data.source.FirebaseDB;
import comchatory.rainvic.chatory.data.model.Story;
import comchatory.rainvic.chatory.util.Utils;

import static comchatory.rainvic.chatory.data.model.StoryLine.CONTENT_TYPE_TEXT;

/**
 * Created by rainvic on 6/30/17.
 */

public class StoryViewModel extends BaseObservable {
    private Context context;
//    private List<StoryLine> storyLines;
    private StoryLineUpdateCallBack storyLineUpdateCallBack;

    private FirebaseDB firebaseDB;

    private Story story;

    private List<Character> characters;

    private Character leadCharacter;

    private String typed = "";

    public StoryViewModel(Context context, Story story, Character leadCharacter, List<Character> characters, StoryLineUpdateCallBack callBack) {
        this.context = context;
        this.story = story;
        this.leadCharacter = leadCharacter;
        this.characters = characters;
        this.firebaseDB = FirebaseDB.getINSTANT(context);
        this.storyLineUpdateCallBack = callBack;
    }

//    public List<StoryLine> getStoryLines() {
//        return storyLines;
//    }

    @Bindable
    public String getEditTextStr(){
        return typed;
    }

    public void setEditTextStr(String typed){
        this.typed = typed;
    }

    public void clickSend(){

        int charNum = (int)(Math.random()*10)%characters.size()+1;
        StoryLine storyLine = new StoryLine((charNum==characters.size())?leadCharacter.getCharacterName():characters.get(charNum).getCharacterName(), CONTENT_TYPE_TEXT, typed, null, Utils.getCurrentUTCTimeStamp(), Utils.getCurrentUTCTimeStamp());
//        if(storyLines == null)
//            storyLines = new ArrayList<>();

//        storyLines.add(storyLine);

        firebaseDB.writeNewStoryLine(story, storyLine);

        if(storyLineUpdateCallBack != null) {
            List<StoryLine> newlyAdded = new ArrayList<>();
            newlyAdded.add(storyLine);
            storyLineUpdateCallBack.onStoryLineUpdated(newlyAdded);
            this.typed = "";
            notifyPropertyChanged(BR.editTextStr);
        }
    }

    public interface StoryLineUpdateCallBack{
        public void onStoryLineUpdated(List<StoryLine> newStoryLines);
        public void onStoryLineUpdatedForNewStoryLines(List<StoryLine> storyLines);
    }
}
