package comchatory.rainvic.chatory.data.model;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rainvic on 7/8/17.
 */

public class Story {
    private String title;
    private long numOfStoryLines;
    private long createdAt;
    private long modifiedAt;
    private long numOfStart;
    private long numOfCommited;
    private long numOfFinidhed;

    public Story() {
    }

    public Story(String title, long numOfStoryLines, long createdAt, long modifiedAt, long numOfStart, long numOfCommited, long numOfFinidhed) {
        this.title = title;
        this.numOfStoryLines = numOfStoryLines;
        this.createdAt = createdAt;
        this.modifiedAt = modifiedAt;
        this.numOfStart = numOfStart;
        this.numOfCommited = numOfCommited;
        this.numOfFinidhed = numOfFinidhed;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getNumOfStoryLines() {
        return numOfStoryLines;
    }

    public void setNumOfStoryLines(long numOfStoryLines) {
        this.numOfStoryLines = numOfStoryLines;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public long getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(long modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public long getNumOfStart() {
        return numOfStart;
    }

    public void setNumOfStart(long numOfStart) {
        this.numOfStart = numOfStart;
    }

    public long getNumOfCommited() {
        return numOfCommited;
    }

    public void setNumOfCommited(long numOfCommited) {
        this.numOfCommited = numOfCommited;
    }

    public long getNumOfFinidhed() {
        return numOfFinidhed;
    }

    public void setNumOfFinidhed(long numOfFinidhed) {
        this.numOfFinidhed = numOfFinidhed;
    }


    // [START post_to_map]
    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("title", title);
        result.put("numOfStoryLines", numOfStoryLines);
        result.put("createdAt", createdAt);
        result.put("modifiedAt", modifiedAt);
        result.put("numOfStart", numOfStart);
        result.put("numOfCommited", numOfCommited);
        result.put("numOfFinidhed", numOfFinidhed);

        return result;
    }
    // [END post_to_map]
}
