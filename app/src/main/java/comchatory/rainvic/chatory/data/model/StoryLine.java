package comchatory.rainvic.chatory.data.model;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rainvic on 6/28/17.
 */

public class StoryLine {
    private String who;
    private int contentType;
    private String content;
    private String contentImg;
    private long createdAt;
    private long modifiedAt;

    /********************************************************
    Content Type
    *********************************************************/
    public static final int CONTENT_TYPE_TEXT = 0;
    public static final int CONTENT_TYPE_IMG = 1;
    public static final int CONTENT_TYPE_IMG_N_TEXT = 2;

    public StoryLine(String who, Integer contentType, String content, String contentImg, Long createdAt, Long modifiedAt) {
        this.who = who;
        this.contentType = contentType;
        this.content = content;
        this.contentImg = contentImg;
        this.createdAt = createdAt;
        this.modifiedAt = modifiedAt;
    }

    public String getWho() {
        return who;
    }

    public void setWho(String who) {
        this.who = who;
    }

    public int getContentType() {
        return contentType;
    }

    public void setContentType(int contentType) {
        this.contentType = contentType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContentImg() {
        return contentImg;
    }

    public void setContentImg(String contentImg) {
        this.contentImg = contentImg;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public long getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(long modifiedAt) {
        this.modifiedAt = modifiedAt;
    }


    // [START post_to_map]
    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("who", who);
        result.put("contentType", contentType);
        result.put("content", content);
        result.put("contentImg", contentImg);
        result.put("createdAt", createdAt);
        result.put("modifiedAt", modifiedAt);

        return result;
    }
    // [END post_to_map]
}
