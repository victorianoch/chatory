package comchatory.rainvic.chatory.data.source;

import android.content.Context;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import comchatory.rainvic.chatory.data.model.Story;
import comchatory.rainvic.chatory.data.model.StoryLine;

/**
 * Created by rainvic on 7/8/17.
 */

public class FirebaseDB {

    public static FirebaseDB INSTANT;
    private Context context;

    private FirebaseDatabase database;
    private DatabaseReference storyLineRef;

    private static final String STORYLINE_KEY = "story_lines";
    private static final String STORYS_KEY = "stories";

    public static FirebaseDB getINSTANT(Context context){
        if(INSTANT == null)
            INSTANT = new FirebaseDB(context);

        return INSTANT;
    }

    public FirebaseDB(Context context) {
        this.context = context;
        this.database = FirebaseDatabase.getInstance();
        this.storyLineRef = database.getReference(STORYLINE_KEY);
    }

    public void writeNewStoryLine(Story story, StoryLine newStoryLine){
        String key = storyLineRef.push().getKey();
        Map<String, Object> storyLineValues = newStoryLine.toMap();


        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/" +story.getTitle()+ "/" + key, storyLineValues);
//        childUpdates.put("/user-posts/" + userId + "/" + key, storyLineValues);

        storyLineRef.updateChildren(childUpdates);
    }
}
