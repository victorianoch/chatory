package comchatory.rainvic.chatory.data.model;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rainvic on 7/8/17.
 */

public class Character {
    private String characterName;
    private long createdAt;
    private String avatar;

    public Character() {
    }

    public Character(String characterName, long createdAt, String avatar) {
        this.characterName = characterName;
        this.createdAt = createdAt;
        this.avatar = avatar;
    }

    public String getCharacterName() {
        return characterName;
    }

    public void setCharacterName(String characterName) {
        this.characterName = characterName;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    // [START post_to_map]
    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("characterName", characterName);
        result.put("createdAt", createdAt);
        result.put("avatar", avatar);

        return result;
    }
    // [END post_to_map]
}
