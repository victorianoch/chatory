package comchatory.rainvic.chatory.data.source;

import android.content.Context;
import android.widget.ImageView;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

/**
 * Created by rainvic on 7/8/17.
 */

public class FirebaseStrg {

    public static FirebaseStrg INSTANT;
    private Context context;

    private FirebaseStorage storage;
    private StorageReference mStorageRef;

    private static final String AVATAR_FOLDER_PATH = "avatar/";

    public static FirebaseStrg getINSTANT(Context context){
        if(INSTANT == null)
            INSTANT = new FirebaseStrg(context);

        return INSTANT;
    }

    public FirebaseStrg(Context context) {
        this.context = context;
        this.storage = FirebaseStorage.getInstance();
        this.mStorageRef = storage.getReference();
    }

    public StorageReference getAvatarImgRef(String imageName){
        StorageReference avatarRef = mStorageRef.child(AVATAR_FOLDER_PATH + imageName);
        return avatarRef;
    }


}
